#!/bin/bash

yum upgrade ca-certificates #--disablerepo=epel
rpm -Uvh http://mirror.webtatic.com/yum/el6/latest.rpm
rpm -Uvh http://mirrors.coreix.net/fedora-epel/6/x86_64/epel-release-6-8.noarch.rpm
yum -y update

yum -y install mc

yum -y install http://www.percona.com/downloads/percona-release/percona-release-0.0-1.x86_64.rpm
yum -y install Percona-Server-server-55.x86_64 Percona-Server-client-55.x86_64
yum  -y install php56w-fpm php56w-cli php56w-mbstring php56w-xml
yum  -y install php56w-mcrypt php56w-gd
yum  -y install php56w-mysql php56w-curl php56w-memcache php556w-intl

yum  -y install curl nginx git mc screen telnet joe expect memcached

iptables --flush && service iptables save && service iptables restart

mkdir /var/lib/php/session
mkdir /var/lib/php/wsdlcache
chmod o+rw /var/lib/php/wsdlcache /var/lib/php/session

rm /etc/localtime
ln -s /usr/share/zoneinfo/UTC /etc/localtime

cp /opt/www/vagrant/nginx/www.conf /etc/nginx/conf.d/
/etc/init.d/nginx restart

mysql -uroot  -e"SET GLOBAL innodb_fast_shutdown = 0"
service mysql stop
rm -f /var/lib/mysql/ib_logfile[01]
rm /etc/my.cnf
cp /opt/www/vagrant/mysql/my.cnf /etc/my.cnf
service mysql start

#dbus-uuidgen > /var/lib/dbus/machine-id

chkconfig mysql on
chkconfig php-fpm on
chkconfig nginx on
chkconfig --add memcached
chkconfig memcached on

###############################################################

echo "CREATE DATABASE \`psh\` CHARACTER SET utf8 COLLATE utf8_general_ci;" | mysql -uroot
echo "CREATE DATABASE \`psh_megad\` CHARACTER SET utf8 COLLATE utf8_general_ci;" | mysql -uroot
echo "GRANT ALL PRIVILEGES ON *.* TO root@192.168.56.1;" | mysql -uroot
echo "GRANT ALL PRIVILEGES ON *.* TO root@192.168.56.115;" | mysql -uroot
echo "date.timezone = UTC" >> /etc/php.ini

service php-fpm restart
#cd /opt/www/www/
#yes | php composer.phar install

chown vagrant:vagrant /home/vagrant/.bash_profile

echo "192.168.56.115 psh.local" >> /etc/hosts
echo "nameserver 8.8.8.8 " >> /etc/resolv.conf

mkdir -p /tmp/runtime/locks
chmod gua+rw /tmp/runtime/locks
chmod -R 0777 /tmp/runtime/

#ln -s /opt/externals/vendor/phpunit/phpunit/phpunit.php /usr/bin/phpunit